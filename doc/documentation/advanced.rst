.. _advanced:

Advanced topics
---------------

Here is a list of specific advanced topics and functionalities of the
GPAW calculator:

.. toctree::
   :maxdepth: 2

   poisson
   electrodynamics/electrodynamics
   cdft/cdft
   dscf/dscf
   dcdft/dcdft
   xc/exx
   external
   grids
   hyperfine
   lcao/lcao
   mom/mom
   smearing
   ofdft/ofdft
   xc/rpa
   scissors/scissors
   xc/xc
   custom_convergence
