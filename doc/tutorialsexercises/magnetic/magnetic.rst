.. _magnetic:

===================
Magnetic properties
===================

.. toctree::
   :maxdepth: 2

   zfs/zfs
   iron/iron
   spinspiral/spinspiral
